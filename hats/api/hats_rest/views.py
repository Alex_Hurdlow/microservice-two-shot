from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["name",
                  "id",
                  "fabric",
                  "style",
                  "color",
                  "picture",
                  ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric",
        "style",
        "color",
        "picture",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


# Create your views here.

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":

        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]["import_href"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location href"},
                status=404,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_hat(request, id):

    if request.method == "GET":
        hat = Hat.objects.get(id=id)

        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
        )
    else:
        try:
            hat = Hat.objects.get(id=id)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
