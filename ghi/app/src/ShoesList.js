import React, { useEffect, useState } from 'react';

const ShoesList = () => {
    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
            console.log(data.shoes)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoes => {
                    return (
                        <tr key={ shoes.href }>
                            <td>{ shoes.id }</td>
                            <td>{ shoes.manufacturer}</td>
                            <td>{ shoes.model_name }</td>
                            <td>{ shoes.color }</td>
                            <td>{ shoes.picture_url}</td>
                            <td>{ shoes.bin}</td>
                        </tr>
                );
            })}
            </tbody>
        </table>
    );
};


export default ShoesList;
